/*
* Function.prototype.bind
* 当 bind 返回的函数作为构造函数的时候，
* bind 时指定的 this 值会失效，但传入的参数依然生效
* 可参考第3版和第4版
* */

// 第1版，返回函数的模拟实现
Function.prototype.bind1 = function (ctx) {
    var that = this;
    return function () {
        return that.apply(ctx); // return 是因为绑定函数可能有返回值
    }
};

// 第2版，传参的模拟实现
Function.prototype.bind2 = function (ctx) {
    var that = this;
    var args = Array.prototype.slice.call(arguments, 1);
    return function () {
        var bindArgs = Array.prototype.slice.call(arguments);
        return that.apply(ctx, args.concat(bindArgs));
    }
}

// 第3版，构造函数效果的模拟实现
Function.prototype.bind3 = function (ctx) {
    var that = this;
    var args = Array.prototype.slice.call(arguments, 1)
    var ResultFn = function () {
        var bindArgs = Array.prototype.slice.call(arguments)
        return that.apply(this instanceof ResultFn ? this: ctx, args.concat(bindArgs))
    }
    ResultFn.prototype = this.prototype // 将返回函数的prototype改为绑定函数的prototype
    // 修改ResultFn.prototype时this.prototype也被改了。
    return ResultFn;
}

// 第4版，构造函数效果的优化
Function.prototype.bind4 = function (ctx) {
    var that = this;
    var args = Array.prototype.slice.call(arguments, 1);
    var MidFoo = function () { // 引入一转化函数

    }
    var ResultFn = function () {
        var bindArgs = Array.prototype.slice.call(arguments);
        return that.apply(this instanceof ResultFn ? this: ctx, args.concat(bindArgs));
    }
    MidFoo.prototype = this.prototype;
    ResultFn.prototype = new MidFoo(); // 修改ResultFn.prototype时改的就不再是this.prototype了
    return ResultFn;
}

// 第5版，最后的优化
Function.prototype.bind = Function.prototype.bind || function (ctx) {
    if(typeof this !== 'function'){
        throw new Error('Function.prototype.bind - what is trying to be bound is not callable')
    }
    var that = this;
    var args = Array.prototype.slice.call(arguments, 1)
    var MidFun = function () {

    }
    var ResultFn = function () {
        var bindArgs = Array.prototype.slice.call(arguments)
        return that.apply(this instanceof ResultFn ? this: ctx, args.concat(bindArgs));
    }
    MidFun.prototype = this.prototype;
    ResultFn.prototype = new MidFun();
    return ResultFn;
}
