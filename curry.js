/*
* 函数柯里化
* */

// 通用版
function curry(fn, args) {
    var length = fn.length;
    var args = args || [];
    return function () {
        var newArgs = args.concat(Array.prototype.slice.call(arguments)); // 每执行一次都往newArgs里推入几个参数
        if (newArgs.length < length){
            return curry.call(this, fn, args.concat(newArgs));
        } else {
            return fn.apply(this, newArgs) // 当参数个数是原来函数的参数个数时调用原来函数
        }
    }
}

// ES6写法
const curry = (fn, arr = []) => (...args) => (
    args => args.length === fn.length ? fn(...args): curry(fn, args)
)([...arr, ...args]);
