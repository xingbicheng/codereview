/*
* 防抖(debouncing)
* 当一次事件发生后，事件处理器要等一定阈值的时间，如果这段时间过去后 再也没有事件发生，
* 就处理最后一次发生的事件。假设还差 0.01 秒就到达指定时间，这时又来了一个事件，
* 那么之前的等待作废，需要重新再等待指定时间。
* 多用于搜索框
* */

function debouncing(fn, wait = 50, immediate) {
    let timer;
    return function () {
        if (immediate){
            fn.apply(this, arguments);
        }
        if (timer){
            clearTimeout(timer);
        }
        timer = setTimeout(() => {
            fn.apply(this, arguments);
        }, wait);
    }
}

// 简单的防抖动函数
// 实际想绑定在 scroll 事件上的 handler
function realFunc(){
    console.log("Success");
}

/*
* 目的：在滚动停止后触发realFunc函数
* */

// 没采用防抖动，每滚动1单位都触发realFunc函数
window.addEventListener('scroll',realFunc);

// 采用了防抖动
// 0.5秒内未滚动才会触发realFunc函数
window.addEventListener('scroll', debouncing(realFunc,500));
