/*
* 深拷贝
* */

var someObj = {
    name: 'xbc',
    age: 20,
    say: function () {
        console.log(this.name);
    }
}
var newObj = JSON.parse(JSON.stringify(someObj));

function deepCopy(obj) {
    var result;
    if (typeof obj === 'object'){
        result = Object.prototype.toString.call(obj) === '[object Array]' ? []: {};
        for(var key in obj){
            result[key] = typeof obj[key] === 'object' ? deepCopy(obj[key]): obj[key];
        }
    } else {
        result = obj;
    }
    return result;
}
