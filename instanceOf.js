/*
* instanceOf
* obj instanceOf Object
* 验证 Object.prototype 是否在 obj 的原型链上
* */

function instanceOf(left, right) {
    let proto = left.__proto__;
    let prototype = right.prototype;
    while (true){
        if (proto === null){
            return false;
        }
        if (proto === prototype){
            return true;
        }
        proto = proto.__proto__; // 一层一层向上检索左边的
    }
}
