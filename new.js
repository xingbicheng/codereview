/*
* new
* */

function New(func){
    var res = {};
    if(func.prototype !== null){
        res.__proto__ = func.prototype;
    }
    var ret = func.apply(res, Array.prototype.slice.call(arguments, 1));
    if((ret !== null) && (typeof ret === 'function' || typeof ret === 'object')){
        return ret;
    }
    return res;
}
