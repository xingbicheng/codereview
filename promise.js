/*
* 原声js实现一个简单的promise
* */

function MyPromise(fn) {
    let that = this;
    that.status = 'pending'
    that.resolveValue = undefined;
    that.rejectValue = undefined;
    function resolve(value) {
        if (that.status === 'pending'){ // 只有pending状态时才可以变化status
            that.status = 'resolved';
            that.resolveValue = value;
        }
    }
    function reject(value) {
        if (that.status === 'pending'){ // 只有pending状态时才可以变化status
            that.status = 'rejected';
            that.rejectValue = value;
        }
    }
    try {
        fn(resolve, reject);
    } catch (e) {
        reject(e);
    }
}

MyPromise.prototype.then = function (onFullfilled, onRejected) {
    let that = this; // then方法被调用时，this指向 p
    switch (that.status) {
        case "resolved":
            onFullfilled(that.resolveValue);
            break;
        case "rejected":
            onRejected(that.rejectValue);
            break;
        default:
    }
}

/*
* 测试
* */

var p = new MyPromise(function (resolve, reject) {
    resolve(1);
});
p.then(function (value) {
    console.log(value);
})
