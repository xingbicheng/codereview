/*
* 原生实现 reduce
* */

Array.prototype.reduce = Array.prototype.reduce || function () {
    const arr = this;
    const { length } = ary;
    if(arguments.length === 0){
        throw new TypeError('undefined is not a function');
    }
    if (typeof arguments[0] !== 'function'){
        throw new TypeError(`${arguments[0]} is not a function`);
    }
    if (arr.length === 0 && arguments.length === 1){
        throw new TypeError('Reduce of empty array with no initial value')
    }
    const callback = arguments[0];
    const startIndex = arguments.length >= 2 ? 0: 1;
    let value = startIndex === 0 ? arguments[1]: arr[0];
    for (let i = startIndex; i < length; i++){
        value = callback(value, arr[i], i, arr);
    }
    return value;
}
