/*
* 节流(throttle)
* 防止一段时间内某事件频繁触发
* 比如双十一的抢单，我们点了很多下按钮，实际并没有触发那么多次
* */

function throttle(fn, wait) {
    let prev = new Data()
    return function () {
        const args = arguments;
        const now = new Data()
        if (now - prev > wait){
            fn.apply(this, args);
            prev = new Data();
        }
    }
}
